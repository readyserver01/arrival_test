import api_methods
import json


def test_1_delete(expect_code, headers_login, url_request):
    res = api_methods.method_delete(url=url_request['bear'], headers=headers_login)
    res_text = res.content.decode("utf-8")
    assert 'OK' == res_text
    assert expect_code['OK'] == res.status_code


def test_2_delete(clean_db, bear, expect_code, headers_login, url_request):
    api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    res = api_methods.method_delete(url=url_request['bear'], headers=headers_login)
    assert expect_code['OK'] == res.status_code
    res = api_methods.method_get(url=url_request['bear'], headers=headers_login)
    res = res['response_content'].decode("utf-8")
    assert '[]' == res


def test_3_delete(clean_db, bear, expect_code, headers_login, url_request):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"]
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_delete(url=url, headers=headers_login)
    assert expect_code['OK'] == res.status_code
    res = api_methods.method_get(url=url, headers=headers_login)
    res = res['response_content'].decode("utf-8")
    assert 'EMPTY' == res



