import api_methods
import json


def test_1_get(clean_db, url_request, expect_code, headers_login):
    res = api_methods.method_get(url=url_request['bear'], headers=headers_login)
    assert expect_code['OK'] == res['response_code']


def test_2_get(clean_db, url_request, expect_code, headers_login):
    res = api_methods.method_get(url=url_request['bear'], headers=headers_login)
    res = res['response_content'].decode("utf-8")
    assert '[]' == res


def test_3_get(add_db, bears_types, url_request, expect_code, headers_login):
    res = api_methods.method_get(url=url_request['bear'], headers=headers_login)
    num_bear = len(json.loads(res['response_content']))
    assert num_bear == len(bears_types)
    assert expect_code['OK'] == res['response_code']


def test_4_get(clean_db, bear, url_request, expect_code, headers_login):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"] + 1
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_get(url=url, headers=headers_login)
    res = res['response_content'].decode("utf-8")
    assert 'EMPTY' == res


def test_5_get(bear, clean_db, url_request, expect_code, headers_login):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"]
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_get(url=url, headers=headers_login)
    res = json.loads(res["response_content"])
    assert res["bear_type"] == bear["bear_type"]



