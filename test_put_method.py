import api_methods
import json


def test_1_put(clean_db, bear, fake_bear_type, expect_code, headers_login, url_request):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"]
    fake_bear_type["bear_type"] = bear["bear_type"]
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_put(url=url, headers=headers_login, data=json.dumps(fake_bear_type))
    assert res == expect_code['OK']
    res = api_methods.method_get(url=url, headers=headers_login)
    res = json.loads(res["response_content"])
    assert res["bear_name"] == fake_bear_type["bear_name"]


def test_2_put(clean_db, fake_bear_type, bear, expect_code, headers_login, url_request):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"]
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_put(url=url, headers=headers_login, data=json.dumps(fake_bear_type))
    assert res == expect_code['OK']
    res = api_methods.method_get(url=url, headers=headers_login)
    res = json.loads(res["response_content"])
    assert res["bear_type"] == bear["bear_type"]


def test_3_put(clean_db, fake_bear_type, bear, expect_code, headers_login, url_request):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"] + 1
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_put(url=url, headers=headers_login, data=json.dumps(fake_bear_type))
    assert res == expect_code['Internal_Server_Error']
