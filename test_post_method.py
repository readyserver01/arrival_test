import api_methods
import json


def test_1_post(bear, clean_db, expect_code, headers_login, url_request):
    res = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    assert expect_code['OK'] == res['response_code']


def test_2_post(bear, clean_db, expect_code, headers_login, url_request):
    api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    res = api_methods.method_get(url=url_request['bear'], headers=headers_login)
    bear_bd = json.loads(res['response_content'])[0]
    print(bear_bd)
    bear_bd.pop('bear_id', 0)
    assert bear == bear_bd


def test_3_post(bear, add_db, expect_code, headers_login, url_request):
    bear_id = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(bear))
    bear_id = bear_id["response_content"]
    url = url_request['bear'] + '/{}'.format(bear_id)
    res = api_methods.method_get(url=url, headers=headers_login)
    bear_bd = json.loads(res['response_content'])
    bear_bd.pop('bear_id', 0)
    assert bear == bear_bd


def test_4_post(clean_db, fake_bear_type, url_request, expect_code, headers_login):
    res = api_methods.method_post(url=url_request['bear'], headers=headers_login, data=json.dumps(fake_bear_type))
    assert expect_code['Internal_Server_Error'] == res['response_code']
    res = api_methods.method_get(url=url_request['bear'], headers=headers_login)
    res = res['response_content'].decode("utf-8")
    assert '[]' == res

