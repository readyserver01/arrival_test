#!/usr/bin/python3
import requests
import json


def method_get(url, headers):
    try:
        api_data = requests.get(url=url, headers=headers)
        print(api_data.headers)
        api_content = api_data.content
        print(" ")
        print("---------------------------------------------------")
        print("method get", api_data.status_code)
        print(url)
        print("response", api_content[0:100])
        print()
        response = {"response_code": api_data.status_code, "response_content": api_content,
                    "response_headers": api_data.headers}
        return response
    except requests.exceptions.HTTPError:
        print('Oops. HTTP Error occured')


def method_patch(url, data, headers):
    try:
        api_data = requests.patch(url=url, data=data, headers=headers)
        print(" ")
        print("---------------------------------------------------")
        print("method patch", api_data.status_code)
        print("request data", data)
        print(url)
        print("response", api_data.content)
        print()
        try:
            api_content = json.loads(api_data.content.decode('utf-8'))
            response = {"response_code": api_data.status_code, "response_content": api_content, "response_headers": api_data.headers}
            return response
        except json.decoder.JSONDecodeError:
            return api_data.status_code
    except requests.exceptions.HTTPError:
        print('Oops. HTTP Error occured')


def method_put(url, data, headers):
    try:
        api_data = requests.put(url=url, data=data, headers=headers)
        print(" ")
        print("---------------------------------------------------")
        print("method put", api_data.status_code)
        print("request data", data)
        print(url)
        print("response", api_data.content)
        print()
        try:
            api_content = json.loads(api_data.content.decode('utf-8'))
            response = {"response_code": api_data.status_code, "response_content": api_content, "response_headers": api_data.headers}
            return response
        except json.decoder.JSONDecodeError:
            return api_data.status_code
    except requests.exceptions.HTTPError:
        print('Oops. HTTP Error occured')


def method_delete(url, headers):
    try:
        api_data = requests.delete(url=url, headers=headers)
        print(" ")
        print("---------------------------------------------------")
        print("method delete", api_data.status_code)
        print(url)
        print("response", api_data.content)
        print()
        try:
            api_content = json.loads(api_data.content.decode('utf-8'))
            return api_content
        except json.decoder.JSONDecodeError:
            print("-----", api_data)
            return api_data
    except requests.exceptions.HTTPError:
        print('Oops. HTTP Error occured')


def method_post(url, data, headers):
    try:
        api_data = requests.post(url=url, data=data, headers=headers)
        print(" ")
        print("---------------------------------------------------")
        print("method post", api_data.status_code)
        print("request", data)
        print(url)
        print("response", api_data.content)
        print()
        response = {"response_code": api_data.status_code, "response_content": api_data.content,
                    "response_headers": api_data.headers}
        try:
            api_content = json.loads(api_data.content.decode('utf-8'))
            response["response_content"] = api_content
            return response
        except json.decoder.JSONDecodeError:
            print("Ответ не json")
            return response
    except requests.exceptions.HTTPError:
        print('Oops. HTTP Error occured')

