#!/usr/bin/python3
import pytest
import json
import requests
import random
from faker import Faker
fake = Faker('en_US')


"  -----------------------------------------url START------------------------------"
server_name = 'dev'
# server_name = os.environ["SERVER"] возможно брать значение из переменных окружения операционной системы


@pytest.fixture()
def url_api():
    url_api = {"dev": "http://172.17.0.1:8091"}
    return url_api[server_name]


@pytest.fixture()
def url_request(url_api):
    url_api = {"info": f"{url_api}/info",
               "bear": f"{url_api}/bear"}
    return url_api


"  ----------------------------------------- headers------------------------------"


@pytest.fixture()
def headers_login():
    headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
    return headers


"  ---------------------------------------services fixtures -------------------------"


@pytest.fixture()
def bears_types():
    types = ["POLAR", "BROWN", "BLACK", "GUMMY"]
    return types


@pytest.fixture()
def clean_db(request, url_request):
    print("clean_db")
    try:
        requests.delete(url=url_request['bear'])
    except requests.exceptions.HTTPError:
        print('Не удалось подготовить БД')

    def clean_db_after():
        print("clean_db_after")
        try:
            requests.delete(url=url_request['bear'])
        except requests.exceptions.HTTPError:
            print('Не удалось очистить БД')

    request.addfinalizer(clean_db_after)


@pytest.fixture()
def add_db(request, url_request, bear, bears_types):
    print("db")
    try:
        for i in bears_types:
            headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
            requests.post(url=url_request['bear'], headers=headers, data=json.dumps(bear))
            print(bear)
    except requests.exceptions.HTTPError:
        print('Не удалось подготовить БД')

    def clean_db_after():
        print("clean_db_after")
        try:
            requests.delete(url=url_request['bear'])
        except requests.exceptions.HTTPError:
            print('Не удалось очистить БД')

    request.addfinalizer(clean_db_after)


@pytest.fixture()
def expect_code():
    codes = {"OK": 200, "Bad_Request": 400, "Internal_Server_Error": 500}
    return codes


@pytest.fixture()
def bear(bears_types):
    bear_type = random.choice(bears_types)
    bear_name = str(fake.name()).upper()
    bear_age = random.randint(1, 99)
    user = {"bear_type": bear_type, "bear_name": bear_name, "bear_age": bear_age}
    return user


@pytest.fixture()
def fake_bear_type():
    user = {"bear_type": "BLACKTEST", "bear_name": "BEARTEST", "bear_age": 14.5}
    return user

